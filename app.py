# Generic package imports
from flask import Flask, render_template, request, session, Markup, current_app, jsonify
from flask_socketio import emit, SocketIO
import requests
import logging
import os
import json
import uuid
import wave
from flask_babel import gettext
socket_app = Flask(__name__)
socket_app.config['SECRET_KEY'] = 'D3L0iTT3'
socket_app.config['FILEDIR'] = 'static/_files/'
socketio = SocketIO(socket_app)
from utility.ASR import transcribe_file
# Set the logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# HOST AND PORT SETTINGS
server_host = os.environ.get('SERVER_HOST') or 'localhost'
server_port = os.environ.get('SERVER_PORT') or '8000'

middle_layer_host = os.environ.get("MIDDLE_LAYER_HOST") or "localhost"
middle_layer_port = os.environ.get("MIDDLE_LAYER_PORT") or "5000"

# #ID's
# session['g_session_id'] = ''
# session['g_bot_id'] = ''

# Validate the id's here
# todo there could be a scenario where one thread completes faster than another. must force to be synchronous
def validate_id(type, id):
    post_request_url = 'http://' + server_host + ':' + server_port + '/validate/' + type +'/' + id
    response = requests.get(post_request_url)
    response_json = json.loads(response.text)
    response_status = response_json['result']
    if response_status == 1:
        return 1, response_json['msg']
    else:
        return 0, response_json['msg']
    pass

@socketio.on('disconnect')
def HandleDisconnect():
    #print('---DISCONNECTING---')
    #handleEnd({"bot_id":session['g_bot_id'],"session_id":session['g_session_id']})
    return

@socketio.on('start-recording')
def start_recording(options):
    # print('START RECORDING, SESSION ID - ', session_id)
    """Start recording audio from the client."""
    # session = get_context(session_id)
    id = uuid.uuid4().hex  # server-side filename
    session['wavename'] = id + '.wav'
    wf = wave.open(current_app.config['FILEDIR'] + session['wavename'], 'wb')
    wf.setnchannels(options.get('numChannels', 1))
    wf.setsampwidth(options.get('bps', 16) // 8)
    wf.setframerate(options.get('fps', 44100))
    session['wavefile'] = wf #todo try storing in mongoDB
    # update_context(session_id, session)

@socketio.on('write-audio')
def write_audio(data):
    # print('WRITING AUDIO, SESSION ID - ', session_id)
    """Write a chunk of audio from the client."""
    # session = get_context(session_id)
    print(f"Data received {data}")
    session['wavefile'].writeframes(data)
    # update_context(session_id, session)

@socketio.on('end-recording')
def end_recording(data):
    # print('END AUDIO, SESSION ID - ', session_id)
    # session = get_context(session_id)
    """Stop recording audio from the client."""
    # print('GOING TO CALL GOOGLE S2T API')
    print("GRV ID", data['grv_id'])
    if data['grv_id'] in [ 'null', 'None', None , 'undefined', ""]:
        return
    statinfo = os.stat(current_app.config['FILEDIR']+session['wavename'])
    if statinfo.st_size == 0:
        return jsonify(
            #{"message":"Ask a question", "state":""}
            message = gettext('Sorry, I could not get your audio input. Could you try again?'),
            state = '',
            ASR = 'True'
        )
    transcript_text = transcribe_file(current_app.config['FILEDIR']+session['wavename'])
    print(transcript_text)

    if str(transcript_text) != 'None':
        emit('add-wavefile', transcript_text)
    session['wavefile'].close()
    #delete the file
    try:
        pass
        os.remove(current_app.config['FILEDIR']+session['wavename'])
    except Exception as e:
        print(e)


    # del session['wavefile']
    # del session['wavename']
    # GOING TO SEND THE TRANSCRIPTED MESSAGE TO ELASTICSEARCH

    # execution_engine = ExecutionEngine(session['session_id'])
    # update_context(session_id,session)

    # bot_id = str(request.args.get('bot_id'))
    bot_id = data['bot_id']
    session_id = data.get("session_id") or None
    print('bot id and session id ', data['bot_id'], data.get("session_id"))
    # Validate Bot_id - POST request
    response_status, message = validate_id('bot', bot_id)
    if response_status == 1:
        logger.error(message)
        print(message)
    else:
        # Get the session id from UI parameter
        # Validate session_id - POST request
        #response_status, message = validate_id('session', session_id)
        response_status = 1
        # if session_id is invalid then get new session_id

        if response_status == 1:
            logger.info('Original session from UI cookie expired - ' + message)
            # get_request_url = 'http://' + server_host + ':' + server_port + '/bot/' + bot_id + '/init'
            # response = json.loads(requests.get(get_request_url).text)
            #
            # # Initialize session_id
            # if str(response['status']) == 'Success':
            #     print('SESSION ID SUCCESS')
            #     session_id = str(response['session_id'])
            #     emit('session', session_id)
            # else:
            #     logger.info(message)
            #     print('SESSION ID FAILURE - ', message)
            if str(transcript_text) != 'None':
                print('Message sent as ASR text')
                handleMessage({"grv_id": data['grv_id'], "message":transcript_text,'session_id':session_id,'bot_id':bot_id,'ASRFlag':True})
            else:
                handleMessage({"grv_id": data['grv_id'], "message":None,'session_id':session_id,'bot_id':bot_id,'ASRFlag':True})
        else:
            if str(transcript_text) != 'None':
                print('Message sent as ASR text')
                handleMessage({"grv_id": data['grv_id'], "message":transcript_text,'session_id':session_id,'bot_id':bot_id,'ASRFlag':True})
            else:
                handleMessage({"grv_id": data['grv_id'], "message":None,'session_id':session_id,'bot_id':bot_id,'ASRFlag':True})


@socketio.on('end')
def handleEnd(params):
    # url = "http://localhost:8000/bot/5b106f08e0923321209e65bd/5b184a22e092332560db492b/end"
    print('END HERE')
    # bot_id = str(request.args.get('bot_id'))
    bot_id = params['bot_id']
    return
    # Validate Bot_id - POST request
    response_status, message = validate_id('bot', bot_id)
    if response_status == 1:
        logger.error(message)
        print(message)
    else:
        # Get the session id from UI parameter
        # session_id = str(request.args.get('session_id'))
        # print('--PARAMS--', params)
        session_id = params['session_id']
        print('---CHECKING SESSION ID---', session_id)
        # Validate session_id - POST request
        # response_status, message = validate_id('session', session_id)

        # if session_id is invalid then get new session_id
        if response_status == 1:
            logger.info('Original session from UI cookie expired - ' + message)
        else:

            url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/' + session_id + '/end'
            response = requests.request("POST", url)
            response_json = json.loads(response.text)
            print('--END MSG RESPONSE JSON - ', response_json['msg'])
            # logger.log(response_json['result']['text'])

@socketio.on('connect')
def handleConnect():
    # Get the bot_id
    print("\n\n[Received Parameters]: ", request.args)
    print(f"[Entered][GRV ID: {request.args['grv_id']}] Connect ....................")
    print("[Entered][GRV ID: {requests.args['grv_id']}] in handle connect")

    grv_id = str(request.args['grv_id']).strip('/"')
    if grv_id in [ 'null', 'None', None , 'undefined', "" ]:
        #, '07fa5afa-e1b0-41cf-8b5d-4561aae0b123', '2cdc61f1-0798-4332-8e5f-ef3cf34fb1f2', '1d10e1fc-b265-4f97-a95a-64ca784c3b91']:
        return

    # extract market/group value
    group = ""
    market = request.args.get("market", {})
    if not isinstance(market, dict):
       market = json.loads(market)
    group = market.get("country", "")

    bot_id = str(request.args.get('bot_id'))
    # bot_id = '5b169827e0923312ccbd27f3'

    # Validate Bot_id - POST request
    response_status, message = validate_id('bot',bot_id)
    print(f"[Validated Bot Id ][GRV ID: {request.args['grv_id']}] :  {response_status} , {message}")
    if response_status == 1:
        logger.error(message)
        print('for bot id ',bot_id,' ',message)
        pass
    else:
        # Get the session id from UI parameter
        print(request.args)
        session_id =  None
        #grv_id = str(request.args.get('grv_id'))
        print(f"[Validated BOT ID][GRV ID: {request.args['grv_id']}]")
        print("Grv id forund: ", grv_id)
        print(f"[GRV ID: {request.args['grv_id']}] --CHECKING SESSION ID CONNECT BEFORE VALIDATE--", session_id)
        #Validate session_id - POST request
        #response_status, message = validate_id('session',session_id)
        response_status = 1

        # if session_id is invalid then get new session_id
        if response_status == 1:
            print(f"[SENDING to MIddle Layer][GRV ID: {request.args['grv_id']}]")
            #logger.info('Original session from UI cookie expired - '+message)
           # print('--SESSION WAS EXPIRED---')
            # Send http GET to initialize session
            get_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/init'
            #response = json.loads(requests.get(get_request_url).text)

            # Initialize session_id
            #print(response)
            # if str(response['status']) == 'Success':
            #     print('SESSION ID SUCCESS')
            #     session_id = str(response['session_id'])
            #     #emit('session', session_id)
            # else:
            #     #logger.info(message)
            #     #print('SESSION ID FAILURE - ',message)
            #     pass
            #print('--CHECKING SESSION ID IF IT WAS INVALID--', session_id)
            #post_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/' + session_id + '/chat'
            #post_request_data = json.dumps({"message":"", "state":"","ASR":False})
            session['g_bot_id'] = bot_id
            session['g_session_id'] = session_id
            # sending post request and saving response as response object
            # response = requests.post(url=post_request_url, data=post_request_data)
            #headers = {'content-type': "application/json"}
            #response = requests.request("POST", post_request_url, data=post_request_data, headers=headers)
            #response_json = json.loads(response.text)
            # print('MSG RESPONSE JSON - ',response_json['result']['message'][0])
            # try:
            #     if response_json['status'] == 'Success':
            #         if response_json['result']['is_multi'] == False:
            #             emit('interaction_message',response_json['result']['message'][0])#[0]['text'])
            #             if response_json['result']['message'][0]['tts'] == True:
            #                 print('EMITING TTS')
            #                 emit('audioTTSOutput', response_json['result']['message'][0]['tts audio'])
            #         else:
            #             for message in response_json['result']['message']:
            #                 emit('interaction_message', message)
            #                 if message['tts'] == True:
            #                     emit('audioTTSOutput', message['tts audio'])
            #                 # emit('audioTTSOutput',)
                # except Exception as e:
                #     logger.error(e)

            print(f"[GRV ID: {request.args['grv_id']}] [ SEDNING PAYLOAD to HANDLE MSG] ", {"grv_id":request.args.get('grv_id'), 'message':'','session_id':session_id,'bot_id':bot_id,'ASRFlag':False})

            handleMessage({"grv_id":grv_id, 'message':'','session_id':session_id,'bot_id':bot_id,'ASRFlag':False, 'group': group})

        else:
            print('--CHECKING SESSION ID CONNECT--', session_id)
            handleMessage({"grv_id":grv_id, 'message':'','session_id':session_id,'bot_id':bot_id,'ASRFlag':False, 'group': group})
    pass


@socketio.on('message')
def handleMessage(msg):
    grv_id = str(msg.get('grv_id')).strip('/"')
    if not msg.get("group"):
        # extract market/group value
        market = request.args.get("market", {})
        if not isinstance(market, dict):
           market = json.loads(market)
        msg["group"] = market.get("country", "")

    print(f"[ ENTERED Handle Msg ] [GRV ID: {grv_id}] ")
    print('[IN HANDLE MESSAGE, and the msg is ]', msg)
    if grv_id in [ 'null', 'None', None, 'undefined', "" ]:
        #, '07fa5afa-e1b0-41cf-8b5d-4561aae0b123', '2cdc61f1-0798-4332-8e5f-ef3cf34fb1f2', '1d10e1fc-b265-4f97-a95a-64ca784c3b91']:
        return
    # Validate Bot_id - POST request
    # msg['bot_id'] = '5b169827e0923312ccbd27f3'
    #response_status, message = validate_id('bot',msg['bot_id'])
    print("----- IN HANDLE MESSAGE ----------" , msg)
    asr_flag =0
    response_status = 0
    if response_status == 1:
        #logger.error(message)
        #print('for bot id ',msg['bot_id'],' ',message)
        pass
    else:
        # Validate session_id - POST request
        # response_status, message = validate_id('session',msg['session_id'])
        print('in the handle message bot id is valid')



        post_request_data = {
            "grv_id":msg.get('grv_id') ,
            "session_id":msg.get("session_id") or None,
            "bot_id": msg['bot_id'],
            "group": msg["group"]
        }
        print("\n\n Message is\n\n",msg)
        post_request_data.update({"message": msg['message'], "state": "", "ASR": asr_flag})
        #post_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + msg['bot_id'] + '/' + msg['session_id'] + '/chat'
        session['g_bot_id'] = msg['bot_id']
        session['g_session_id'] = msg.get('session_id') or None

        print(f"[ SENDING PAYLOAD TO MIDDLE LAYER ] [GRV ID: {grv_id}] ")
        print(f"[PAYLOAD ] [GRV ID: {grv_id}]: {post_request_data}")
        post_request_url = "http://{server_host}:{server_port}/endpoint".format(
            server_host=middle_layer_host,
            server_port=middle_layer_port
        )


        print('POST REQUEST URL - ', post_request_url)
        print("POST REQUEST DATA - ", post_request_data)
        headers = {'content-type': "application/json"}
        # if response_status == 1:
        #     logger.error(message)
        #     print('SESSION ID IS INVALID _ ',message)
        #     post_request_data = json.dumps({"message":'', "state": "", "ASR": "false"})
        #     r = requests.request("POST", post_request_url, data=post_request_data, headers=headers) #nothing to emit, just re-execute the current state?

        # else:
        # sending post request and saving response as response object
        post_request_data = json.dumps(post_request_data)
        r = requests.request("POST", post_request_url, data=post_request_data, headers=headers)

        print(r.text)
        r_json = json.loads(r.text)
        print('R JSON',r_json)
        print(f"[RECEIVED DATA FROM SERVER ] [GRV ID: {grv_id}]: {r_json}")
        #print('THE MESSAGE JSON - ',r_json)

        if r_json['status'] == 'Success':
            try:
                if r_json['result']['is_multi'] == False:
                    print('THE HANDLE MESSAGE TO PRINT IS ',r_json['result']['message'][0]['text'])
                    emit('interaction_message', r_json['result']['message'][0])#[0]['text'])
                    if r_json['result']['message'][0]['tts'] == True:
                        emit('audioTTSOutput', r_json['result']['message'][0]['tts audio'])
                else:
                    for message in r_json['result']['message']:
                        emit('interaction_message', message)
                        if message['tts'] == True:
                            emit('audioTTSOutput', message['tts audio'])
                        print('MSSG IS - ',message)

            except Exception as e:
                print('HANDLE MESSAGE ERROR IS ', e)
                emit('message',r_json['result'])
                logger.error(e)

if __name__ == '__main__':
    try:
        app_host = os.environ.get('APP_HOST') or 'localhost'
        app_port = os.environ.get('APP_PORT') or "5003"
        appEvent = socketio.run(socket_app, host=str(app_host), port=int(app_port))
        # eventlet.wsgi.server(eventlet.listen((app_host, app_port)), app)
    except Exception as e:
        logger.error(e)
